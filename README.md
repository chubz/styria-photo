Photo Hut
=========

Do this
-------

- git clone https://chubz@bitbucket.org/chubz/styria-photo.git
- cd styria-photo/
- virtualenv env
- source profile
- pip install -r requirements.txt
- django-admin.py syncdb
- django-admin.py runserver


Project limitations and explanations
------------------------------------
Upload is enabled for anonymous users, but rating however isn't. Users are not integrated in the sites frontend so you should use djangos admin interface to make and login users for this project.



