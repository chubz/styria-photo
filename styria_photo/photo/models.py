from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize
from django.core.urlresolvers import reverse_lazy
from django.dispatch import receiver


def upload_to_path(instance, filename):
    return 'images/{}/{}'.format(instance.author.username if instance.author else 'user_anon', filename)


class Photo(models.Model):
    name = models.CharField(_(u'name'), max_length=50)
    author = models.ForeignKey(User, verbose_name=_(u'author'), null=True, blank=True)
    pub_date = models.DateTimeField(_(u'publication date'), auto_now_add=True)
    image = models.ImageField(_(u'image'), upload_to=upload_to_path, max_length=255)
    image_thumb = ImageSpecField((ResizeToFill(75, 75), ), image_field='image', format='JPEG', options={'quality': 90})
    image_big = ImageSpecField((SmartResize(640, 640), ), image_field='image', format='JPEG', options={'quality': 90})
    description = models.TextField(_(u'description'), max_length=150, blank=True)
    avg_rating = models.FloatField(null=True)

    @property
    def average_rating(self):
        return round(self.avg_rating, 2) if self.avg_rating is not None else _(u'Unrated')

    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Photos')
        ordering = ('-pub_date', )

    def delete(self, *args, **kwargs):
        self.image.storage.delete(self.image.path)
        super(Photo, self).delete(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('home:photo_detail', kwargs={'user': self.author.username if self.author else 'anon', 'pk': self.id})

    def __unicode__(self):
        return self.name


class Rating(models.Model):
    RATING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    image = models.ForeignKey('Photo', verbose_name=_(u'image'))
    rater = models.ForeignKey(User, verbose_name=_(u'rater'))
    rating = models.PositiveSmallIntegerField(_(u'image rating'), choices=RATING_CHOICES)
    pub_date = models.DateTimeField(_(u'publication date'), auto_now_add=True)

    class Meta:
        verbose_name = _('Rating')
        verbose_name_plural = _('Ratings')

    def __unicode__(self):
        return 'Rating for {}'.format(self.image)


@receiver([models.signals.post_save, models.signals.post_delete], sender=Rating)
def update_average(sender, instance, **kwargs):
    instance.image.avg_rating = sender.objects.filter(image=instance.image).aggregate(models.Avg('rating'))['rating__avg']
    instance.image.save()
