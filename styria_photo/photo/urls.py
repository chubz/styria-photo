from django.conf.urls import patterns, url

urlpatterns = patterns('photo.views',
    url(r'^$', 'index', name='index'),
    url(r'^(?P<user>\w+)/$', 'user_photo_list', name='user_photo_list'),
    url(r'^(?P<user>\w+)/(?P<pk>\d+)/$', 'photo_detail', name='photo_detail'),
)
