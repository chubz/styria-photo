from __future__ import absolute_import
from django.views.generic import ListView, CreateView
from .forms import PhotoForm, RatingForm
from .models import Photo, Rating
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from djpjax import PJAXResponseMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse_lazy


class UserPhotoListView(PJAXResponseMixin, ListView):
    template_name = 'photo_list.html'
    paginate_by = 12

    def get_queryset(self):
        if self.kwargs['user'] == 'anon':
            user = None
        else:
            user = get_object_or_404(User, username__iexact=self.kwargs['user'])
        if self.request.GET.get('ordering'):
            return Photo.objects.filter(author=user).order_by(self.request.GET.get('ordering'))
        return Photo.objects.filter(author=user)

    def get_context_data(self, **kwargs):
        print type(self.request.user)
        context = super(UserPhotoListView, self).get_context_data(**kwargs)
        if self.object_list[0].author:
            context['is_author'] = self.request.user == self.object_list[0].author

        return context

user_photo_list = UserPhotoListView.as_view()


class PhotoListView(PJAXResponseMixin, CreateView):
    model = Photo
    form_class = PhotoForm
    template_name = 'photo_list.html'

    def get_context_data(self, **kwargs):
        if self.request.GET.get('ordering'):
            photo_list = Photo.objects.order_by(self.request.GET.get('ordering'))
        else:
            photo_list = Photo.objects.all()
        paginator = Paginator(photo_list, 12)
        page = self.request.GET.get('page')
        try:
            photos = paginator.page(page)
        except PageNotAnInteger:
            photos = paginator.page(1)
        except EmptyPage:
            photos = paginator.page(paginator.num_pages)
        kwargs['page_obj'] = photos
        return super(PhotoListView, self).get_context_data(**kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(PhotoListView, self).form_valid(form)

index = PhotoListView.as_view()


class PhotoDetailView(PJAXResponseMixin, CreateView):
    model = Rating
    form_class = RatingForm
    template_name = 'photo_detail.html'

    def get_success_url(self):
        return reverse_lazy('home:photo_detail', kwargs={'user': self.kwargs['user'], 'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super(PhotoDetailView, self).get_context_data(**kwargs)
        if self.kwargs['user'] != 'anon':
            get_object_or_404(User, username__iexact=self.kwargs['user'])

        context['photo'] = get_object_or_404(Photo, pk=self.kwargs['pk'])
        context['ratings'] = Rating.objects.filter(image=self.kwargs['pk']).order_by('-pub_date')[:5]
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.image = Photo.objects.get(pk=self.kwargs['pk'])
        return super(PhotoDetailView, self).form_valid(form)

photo_detail = PhotoDetailView.as_view()
