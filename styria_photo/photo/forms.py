from __future__ import absolute_import
from .models import Photo, Rating
import floppyforms as forms
from django.utils.translation import ugettext_lazy as _


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ('name', 'image', 'description',)
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': _(u'image name...')}),
            'image': forms.ClearableFileInput(attrs={'placeholder': _(u'upload your image...')}),
            'description': forms.Textarea(attrs={'placeholder': _(u'describe this image in short...'), 'rows': 2, 'cols': 50}),
        }

    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(PhotoForm, self).save(commit=False)
        m.author = self.instance.user if not self.instance.user.is_anonymous() else None
        if commit:
            m.save()
        return m


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ('rating',)
        widgets = {
            'rating': forms.Select(attrs={'id': 'starrating'}),
        }

    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(RatingForm, self).save(commit=False)
        m.rater = self.instance.user
        m.image = self.instance.image
        if commit:
            m.save()
        return m

