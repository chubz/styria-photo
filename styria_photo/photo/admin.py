from __future__ import absolute_import
from django.contrib import admin
from .models import Photo, Rating


class PhotoAdmin(admin.ModelAdmin):
    search_fields = ['name']
    readonly_fields = ('average_rating', )
    fieldsets = (
        (
            'Photo info', {'fields': (('name', 'average_rating'), 'description')}
        ),
        (
            'Photo upload', {'fields': ('image', 'author')}
        ),
    )

admin.site.register(Photo, PhotoAdmin)
admin.site.register(Rating)
