from __future__ import absolute_import
from django.test import TestCase
from django.contrib.auth.models import User
from .models import Photo, Rating
from django.core.urlresolvers import reverse_lazy
from django.test.client import Client


class PhotoModelTestCase(TestCase):
    fixtures = ('project_testdata.json',)

    def test_get_absolute_url(self):
        photo = Photo.objects.get(pk=1)
        self.assertEqual(photo.get_absolute_url(), '/anon/1/')

    def test_average_rating(self):
        photo = Photo.objects.get(pk=2)
        self.assertEqual(photo.average_rating, 1.4)

    def test_update_average(self):
        photo = Photo.objects.get(pk=3)
        Rating.objects.create(image=photo, rater_id=1, rating=3)
        self.assertEqual(photo.average_rating, 3.2)

    def test_average_unrated(self):
        Rating.objects.filter(image_id=4).delete()
        photo = Photo.objects.get(pk=4)
        self.assertEqual(photo.average_rating, 'Unrated')


class PhotoViewTestCase(TestCase):
    fixtures = ('project_testdata.json',)

    def setUp(self):
        User.objects.create_superuser('superTest', 'testinjo@test.tt', 'testpw')
        self.client = Client()

    def test_homepage(self):
        response = self.client.get(reverse_lazy('home:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'photo_list.html')

    def test_user_photo_list(self):
        response = self.client.get(reverse_lazy('home:user_photo_list', kwargs={'user': 'chubz'}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse_lazy('home:user_photo_list', kwargs={'user': 'anon'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'photo_list.html')
        response = self.client.get(reverse_lazy('home:user_photo_list', kwargs={'user': 'isus'}))
        self.assertEqual(response.status_code, 404)

    def test_photo_detail(self):
        # test the page
        response = self.client.get(reverse_lazy('home:photo_detail', kwargs={'user': 'chubz', 'pk': 3}))
        self.assertEqual(response.status_code, 200)

        # test context
        self.assertTrue('photo' in response.context)
        self.assertTrue('ratings' in response.context)

        # test context objects
        self.assertEqual([rating.pk for rating in response.context['ratings']], [10, 9, 8, 7])

        #test template
        self.assertTemplateUsed(response, 'photo_detail.html')

        # test bad page
        response = self.client.get(reverse_lazy('home:photo_detail', kwargs={'user': 'anon', 'pk': 10}))
        self.assertEqual(response.status_code, 404)

        # test no post data
        response = self.client.post(reverse_lazy('home:photo_detail', kwargs={'user': 'anon', 'pk': 4}), {})
        self.assertEqual(response.status_code, 200)

        # test post with valid data
        self.client.login(username='superTest', password='testpw')
        response = self.client.post(reverse_lazy('home:photo_detail', kwargs={'user': 'anon', 'pk': 4}), {'rating': 3})
        self.assertEqual(response.status_code, 302)

        # test dirty post
        response = self.client.post(reverse_lazy('home:photo_detail', kwargs={'user': 'anon', 'pk': 4}), {'foo': 'bar'})
        self.assertEqual(response.status_code, 200)
